import Prelude hiding (map, foldr, foldr1, foldl, foldl1, zipWith)

map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x:xs)  = (f x) : (map f xs)

zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith f _ [] = []
zipWith f [] _ = []
zipWith f (x:xs) (y:ys) = f x y : zipWith f xs ys

{-
fold:: (a -> a -> a) -> [a] -> a
fold f (x:[]) = x
fold f (x:xs) = f(x fold(f xs)) -}

fold:: (a-> b-> b) -> [a] -> b

is_gelijk :: String -> String -> Bool
is_gelijk n = \y -> y == n

owned_games:: [String]
owned_games = ["AC","Persona","Ck3"]


contains_true :: [Bool] -> Bool -> Bool
contains_true _ True = True
contains_true [] False = False
contains_true (x:xs) _ = contains_true xs x

owned :: [String] -> [Bool]
owned [] = []
owned (x:xs) = contains_true (tail owns) (head owns) : owned xs
                where owns = (map (is_gelijk x) owned_games)
