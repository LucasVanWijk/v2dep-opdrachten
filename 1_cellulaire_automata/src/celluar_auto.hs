import Data.List
import Data.Maybe

data Cell = Alive | Dead
    deriving (Eq)

{--De huidige manier van uitvoeren is dat er een lijst wordt gemaakt met type Cell
die lijst wordt geprint en daarvoor heeft hij dus de show instantie. 
Een andere manier is om nadat de lijst is gemaakt de lijst langs tegaan
en aan string een wit blokje toe te voegen bij alive en zwart bij dead

voordeel van de tweede manier is dat er geen comma's staan tussen de blokjes huidige invoer ziet er als volgd uit
[▓,▓,▓,░,▓,▓,▓,▓,▓,░,▓,▓,░,░,░,░]
dit word
[▓▓▓░▓▓▓▓▓░▓▓░░░░

Nadeel is volgens mij dat de complexiteit hoger is voor elke generatie moeten er twee lijsten gemaakt worden in plaats van 1.
Ik heb grotendeels voor de eerste manier gekozen om telaten zien dat ik instances etc snap.
Maar ik ben mij er dus van bewust dat de ander manier ook een oplosing was en waarschijnlijk geprefereerd is -}


instance Show Cell where
    show Alive = "▓"
    show Dead  = "░"



-- /Generates a list of specified length where all indies which correpsond with the values in the given list are equel to true
makeIniList :: [Int] -> Int-> [Cell]
makeIniList aliveIndexes length = [boolToCell (elem index aliveIndexes)| index <- [0.. length]]


-- /For a given bool value returns a cell value
boolToCell:: Bool -> Cell
boolToCell True = Alive
boolToCell False = Dead


allStateCellAndNeigbors :: [[Cell]]
allStateCellAndNeigbors = [[x,y,z] | x<-bin, y<-bin, z<-bin] where bin=[Alive, Dead]

-- | finds the index of the current state in a list of all posible states. 
-- | finds the value of this index in the rule boolean list which represents the new state of the cell 
determinAlive :: [Bool] -> [Cell] -> Cell
determinAlive rule state = boolToCell $ rule !! fromJust (elemIndex state allStateCellAndNeigbors)

-- | Takes a input and returns it's 8 bit boolean representation. Inputs bigger than 256 will overflow
binary :: Int -> [Bool]
binary x = intConvBool (rem x 256) 128 []

-- | Takes a input and returns it's 8 bit boolean representation.
intConvBool :: Int -> Int -> [Bool] -> [Bool]
intConvBool 0 _ [] = [False, False, False, False, False, False, False, False]
intConvBool _ 0 z = z
intConvBool x y z 
  | x - y >= 0 = intConvBool (x-y) (div y 2) (True:z)
  | otherwise = intConvBool x (div y 2) (False:z) 


-- /Given a geration of cells determins the next generation
nextGen :: [Bool] -> Cell -> [Cell] -> [Cell]
nextGen _ _ (x:[]) = [x]
nextGen rule left (self:allRight) = (collapsedSelf : nextGen rule self allRight)
                                where collapsedSelf = determinAlive rule [left,self,(head allRight)]


-- /Given a start generation and how many generations need to be displayed
-- determin the next generations as many times as the value of how many generations need to be displayed
evolve :: Int -> Int -> [Cell] -> IO ()
evolve _ 0 currentGen = putStrLn $ genToString (reverse currentGen) ""
evolve ruleNumber generationsLeft currentGen = do {putStrLn $ genToString (reverse currentGen) ""
                                        ;evolve ruleNumber (generationsLeft - 1) (newGen)}
                                        where newGen = nextGen rule Dead currentGen
                                              rule = binary ruleNumber

-- | Converts a generation (list) of cells to a string where a cell is a char and a white cube represents alive.
-- | a black one represents dead
genToString :: [Cell] -> String -> String
genToString (x:[]) y = y  
genToString (x:xs) y
    | x == Alive = genToString xs "▓" ++ y
    | otherwise = genToString xs "░" ++ y

wow :: Num a => a -> a -> a
wow a = \bib -> a + bib 